﻿using System;

namespace lekcja_14._powtorzenie_materialu_cw_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string imie = "Mariusz";
            int wiek = 15;
            double wzrost = 185.0;
            float przebiegnieteKm = 166.9f;
            char literka = 'A';

            Console.WriteLine("Imię: {0}", imie);
            Console.WriteLine("Wiek: {0}", wiek);
            Console.WriteLine("Wzrost: {0}", wzrost);
            Console.WriteLine("Przebiegnięte km: {0}", przebiegnieteKm);
            Console.WriteLine("Literka: {0}", literka);

            Console.ReadKey();
        }
    }
}
